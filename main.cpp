/*@author gihan tharanga*/

#include <iostream>
#include <string>

#include "opencv2\core\core.hpp"
#include "opencv2\contrib\contrib.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\objdetect\objdetect.hpp"
#include "opencv2\opencv.hpp"

/*header files*/
#include "FaceRec.h"
#include "VideoCap.h"
#include "imgProc.h"

using namespace std;
using namespace cv;

int main()
{	
	//int x = smoothingImage();
	string
		db =          "C:/Users/USER/Desktop/UPN 2016-2/SisIn/FaceReco/FR/data/db.txt",
		cascadeFile = "C:/Users/USER/Desktop/UPN 2016-2/SisIn/FaceReco/FR/data/fr.yml",
		faceDetector= "C:/opencv/sources/data/haarcascades/haarcascade_frontalface_alt2.xml";

	fisherFaceTrainer(
		db,
		cascadeFile,
		faceDetector
	);

	int x = FaceRecognition(cascadeFile, faceDetector);

	system("pause");
	return 0;
}